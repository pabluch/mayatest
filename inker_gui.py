from common.Qt import QtGui, QtCore, QtWidgets
from collections import namedtuple
from logging import getLogger

# import maya.api.OpenMaya as OpenMaya2
# from ctypes import *

logger = getLogger()


class UiWindow(QtWidgets.QMainWindow):
    generateClicked = QtCore.Signal(tuple)
    delClicked = QtCore.Signal()
    GEN_SPIRAL1 = 0
    GEN_SPIRAL2 = 1
    GEN_LINES_H = 2
    GEN_LINES_W = 3


class UiController(QtCore.QObject):
    selectionChanged = QtCore.Signal(list)


def create_window(controller, parent=None):
    row = 1
    window = UiWindow(parent)
    window.setWindowTitle('Inktober generator')

    statusbar = QtWidgets.QStatusBar()
    window.setStatusBar(statusbar)

    container = QtWidgets.QWidget(window)
    layout = QtWidgets.QGridLayout(container)
    container.setLayout(layout)
    window.setCentralWidget(container)

    label = QtWidgets.QLabel('Inker shape:', container)
    item = QtWidgets.QComboBox()

    item.addItem("Single spiral ",      UiWindow.GEN_SPIRAL1)
    item.addItem("Double spiral",       UiWindow.GEN_SPIRAL2)
    item.addItem("Horizontal lines",    UiWindow.GEN_LINES_H)
    item.addItem("Vertical lines",      UiWindow.GEN_LINES_W)
    layout.addWidget(label, row, 0)
    layout.addWidget(item, row, 1)
    row += 1
    tK = item

    label = QtWidgets.QLabel('Image file:', container)
    lay2 = QtWidgets.QGridLayout()
    item = QtWidgets.QLineEdit()
    icon = QtWidgets.QPushButton('Browse')
    lay2.addWidget(item, 1, 0)
    lay2.addWidget(icon, 1, 1)
    layout.addWidget(label, row, 0)
    layout.addLayout(lay2, row, 1)
    row += 1
    tF = item

    def choose_file():
        tF.setText(QtWidgets.QFileDialog.getOpenFileName()[0])
        # print(QtWidgets.QFileDialog.getOpenFileName())

    icon.clicked.connect( choose_file )

    label = QtWidgets.QLabel('Width:', container)
    item = QtWidgets.QSpinBox(container)
    item.setMinimum(1)
    item.setValue(3)
    layout.addWidget(label, row, 0)
    layout.addWidget(item, row, 1)
    row += 1
    tW = item

    label = QtWidgets.QLabel('Height:', container)
    item = QtWidgets.QSpinBox(container)
    item.setMinimum(1)
    item.setValue(3)
    layout.addWidget(label, row, 0)
    layout.addWidget(item, row, 1)
    row += 1
    tH = item

    label = QtWidgets.QLabel('Step:', container)
    item = QtWidgets.QDoubleSpinBox(container)
    item.setMinimum(0.001)
    item.setValue(.01)
    item.setSingleStep(0.001)
    item.setDecimals(3)
    layout.addWidget(label, row, 0)
    layout.addWidget(item, row, 1)
    row += 1
    tX = item

    label = QtWidgets.QLabel('Spins:', container)
    item = QtWidgets.QSpinBox(container)
    item.setMinimum(3)
    item.setValue(40)
    layout.addWidget(label, row, 0)
    layout.addWidget(item, row, 1)
    row += 1
    tI = item

    button = QtWidgets.QPushButton('Del', container)
    layout.addWidget(button, row, 0)
    buttonD = button

    button = QtWidgets.QPushButton('Generate', container)
    layout.addWidget(button, row, 1)
    buttonG = button
    row += 1

    # l = QtWidgets.QLabel('Generate', container)
    # l.setFixedSize(800, 800)
    # layout.addWidget(l, row, 1)



    def onclick():
        Msg = namedtuple('Msg', 'width height step filepath spins opp')
        tup = Msg(
            width= tW.value(),
            height= tH.value(),
            step= tX.value(),
            spins=tI.value(),
            filepath=tF.text(),
            opp=tK.itemData(tK.currentIndex())
        )

        print(tup)
        # image2 = OpenMaya2.MImage()
        # i = image2.readFromFile(tF.text(), 1)
        #
        # buf = c_ubyte * (800*800*4)
        # buf = buf.from_address(long(image2.pixels()))
        # img = QtGui.QImage( buf, 800, 800, QtGui.QImage.Format_RGB32).rgbSwapped ()
        #
        # pix = QtGui.QPixmap.fromImage(img)
        # l.setPixmap(pix)

        window.generateClicked.emit(tup)

    def tidy_up():
        window.delClicked.emit()

    def update_statusbar(newsel):
        if not newsel:
            txt = 'Nothing selected.'
        elif len(newsel) == 1:
            txt = '"%s" selected.' % newsel[0]
        else:
            txt = '%s objects selected.' % len(newsel)
        window.statusBar().showMessage(txt)

    def onselchange(ind):
        # controls = [tW, tH, tX, tF, tI, tRand, tSeed, tInv, tWalls]
        # opp = tK.itemData(ind)
        # for c in controls:
        #     c.setDisabled(True)
        #
        # if opp == UiWindow.GEN_CAVE0 or opp == UiWindow.GEN_CAVE1:
        #     for c in [tW, tH, tX, tF, tI, tRand, tSeed, tInv, tWalls]:
        #         c.setEnabled(True)
        #     if tRand.isChecked():
        #         tSeed.setEnabled(False)
        #
        # elif opp == UiWindow.GEN_CUBE_OLD or opp == UiWindow.GEN_CUBE_20:
        #     pass
        #
        # elif opp == UiWindow.GEN_PAT_1 or opp == UiWindow.GEN_PAT_2:
        #     for c in [tW, tH, tX]:
        #         c.setEnabled(True)
        pass

    controller.selectionChanged.connect(update_statusbar)
    tK.currentIndexChanged.connect(onselchange)

    buttonG.clicked.connect(onclick)
    buttonD.clicked.connect(tidy_up)

    return window


def _pytest():
    import random,sys

    def ongenerate(args):
        def test_sel():
            sels = [
                [],
                ['single'],
                ['single', 'double'],
                ['single', 'double', 'triple'],
            ]
            return random.choice(sels)
        cont.selectionChanged.emit(test_sel())

    try:
        app = QtGui.QApplication(sys.argv)
    except:
        app = QtGui.QApplication.instance()

    cont = UiController()
    win = create_window(cont)
    win.generateClicked.connect(ongenerate)
    win.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    _pytest()
