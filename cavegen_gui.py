from common.Qt import QtGui, QtCore, QtWidgets
from collections import namedtuple


class CaveWindow(QtWidgets.QMainWindow):
    generateClicked = QtCore.Signal(tuple)
    delClicked = QtCore.Signal()
    GEN_CUBE_OLD = 0
    GEN_CUBE_20 = 1
    GEN_CAVE0 = 2
    GEN_PAT_1 = 3
    GEN_PAT_2 = 4
    GEN_CAVE1 = 5
    seed = None


class CaveController(QtCore.QObject):
    selectionChanged = QtCore.Signal(list)


def create_window(controller, parent=None):
    window = CaveWindow(parent)
    window.setWindowTitle('Cave generator')

    statusbar = QtWidgets.QStatusBar()
    window.setStatusBar(statusbar)

    container = QtWidgets.QWidget(window)
    layout = QtWidgets.QGridLayout(container)
    container.setLayout(layout)
    window.setCentralWidget(container)

    label = QtWidgets.QLabel('Generate:', container)
    item = QtWidgets.QComboBox()

    item.addItem("Cave Test ", CaveWindow.GEN_CAVE1);
    item.addItem("Cave Test (debug blobs)", CaveWindow.GEN_CAVE0);
    item.addItem("Pattern 1", CaveWindow.GEN_PAT_1);
    item.addItem("Cube (Maya Python APi 2.0)", CaveWindow.GEN_CUBE_20);
    item.addItem("Cube (old OpenMaya API)", CaveWindow.GEN_CUBE_OLD);
    layout.addWidget(label, 1, 0)
    layout.addWidget(item, 1, 1)
    tK = item

    label = QtWidgets.QLabel('Width:', container)
    item = QtWidgets.QSpinBox (container)
    item.setMinimum(3)
    item.setValue(30)
    layout.addWidget(label, 2, 0)
    layout.addWidget(item, 2, 1)
    tW = item

    label = QtWidgets.QLabel('Height:', container)
    item = QtWidgets.QSpinBox(container)
    item.setMinimum(3)
    item.setValue(20)
    layout.addWidget(label, 3, 0)
    layout.addWidget(item, 3, 1)
    tH = item

    label = QtWidgets.QLabel('Size:', container)
    item = QtWidgets.QDoubleSpinBox(container)
    item.setMinimum(0.1)
    item.setValue(1.0)
    layout.addWidget(label, 4, 0)
    layout.addWidget(item, 4, 1)
    tX = item

    label = QtWidgets.QLabel('Smooth iterations:', container)
    item = QtWidgets.QSpinBox(container)
    item.setMinimum(0)
    item.setValue(2)
    layout.addWidget(label, 5, 0)
    layout.addWidget(item, 5, 1)
    tI = item

    label = QtWidgets.QLabel('Fill percent:', container)
    item = QtWidgets.QSpinBox(container)
    item.setMinimum(0)
    item.setValue(42)
    layout.addWidget(label, 6, 0)
    layout.addWidget(item, 6, 1)
    tF = item

    item = QtWidgets.QCheckBox('Inverted',container)
    item.setChecked(True)
    layout.addWidget(item, 7, 0)
    tInv = item

    item = QtWidgets.QCheckBox('Walls',container)
    item.setChecked(True)
    layout.addWidget(item, 7, 1)
    tWalls = item




    item = QtWidgets.QCheckBox('Random', container)
    item.setChecked(True)
    layout.addWidget(item, 8, 0)
    tRand = item
    def randomClicked( state):
        if state ==0:
            tSeed.setEnabled(True)
        else:
            tSeed.setEnabled(False)
    tRand.stateChanged.connect( randomClicked )


    item = QtWidgets.QLineEdit(container)
    item.setText('My random seed.')
    layout.addWidget(item, 8, 1)
    tSeed = item
    window.seed = tSeed  # for backset from generator
    if tRand.isChecked():
        tSeed.setEnabled(False)

    button = QtWidgets.QPushButton('Del', container)
    layout.addWidget(button, 9, 0)
    buttonD = button

    button = QtWidgets.QPushButton('Generate', container)
    layout.addWidget(button, 9, 1)
    buttonG = button

    def onclick():
        CaveDialog = namedtuple('CaveDialog', 'width height size opp fill_p s_iter inv seed random walls')
        tup = CaveDialog(
            width= tW.value(),
            height= tH.value(),
            size= tX.value(),
            fill_p=tF.value(),
            s_iter=tI.value(),
            inv=tInv.isChecked(),
            random=tRand.isChecked(),
            seed=tSeed.text(),
            walls=tWalls.isChecked(),
            opp=tK.itemData(tK.currentIndex())
        )

        print(tup )
        window.generateClicked.emit(tup)

    def tidy_up():
        window.delClicked.emit()

    def update_statusbar(newsel):
        if not newsel:
            txt = 'Nothing selected.'
        elif len(newsel) == 1:
            txt = '"%s" selected.' % newsel[0]
        else:
            txt = '%s objects selected.' % len(newsel)
        window.statusBar().showMessage(txt)

    def onselchange(ind):
        controls = [tW, tH, tX, tF, tI, tRand, tSeed, tInv, tWalls]
        opp = tK.itemData(ind)
        for c in controls:
            c.setDisabled(True)

        if opp == CaveWindow.GEN_CAVE0 or opp == CaveWindow.GEN_CAVE1:
            for c in [tW, tH, tX, tF, tI, tRand, tSeed, tInv, tWalls]:
                c.setEnabled(True)
            if tRand.isChecked():
                tSeed.setEnabled(False)

        elif opp == CaveWindow.GEN_CUBE_OLD or opp == CaveWindow.GEN_CUBE_20:
            pass

        elif opp == CaveWindow.GEN_PAT_1 or opp == CaveWindow.GEN_PAT_2:
            for c in [tW, tH, tX]:
                c.setEnabled(True)

    controller.selectionChanged.connect(update_statusbar)
    tK.currentIndexChanged.connect(onselchange )

    buttonG.clicked.connect(onclick)
    buttonD.clicked.connect(tidy_up)

    return window


def _pytest():
    import random,sys

    def ongenerate(args):
        def test_sel():
            sels = [
                [],
                ['single'],
                ['single', 'double'],
                ['single', 'double', 'triple'],
            ]
            return random.choice(sels)
        win.seed.setText('aaa')
        cont.selectionChanged.emit(test_sel())

    try:
        app = QtGui.QApplication(sys.argv)
    except:
        app = QtGui.QApplication.instance()

    cont = CaveController()
    win = create_window(cont)
    win.generateClicked.connect(ongenerate)
    win.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    _pytest()
