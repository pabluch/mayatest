import random
import logging
import time

logger = logging.getLogger()


class GeoMaker(object):
    _map = [[]]
    _walk_nums = [[]]
    _map_w = None
    _map_h = None
    _w = None
    _h = None
    _verts = []
    _inds = []
    size = 1.
    inverted = False
    walls = True

    def __init__(self, _map):
        self._map = _map
        self._map_w = len(self._map)
        self._map_h = len(self._map[0])
        self._w = self._map_w - 1
        self._h = self._map_h - 1

    def generate_mesh(self):
        import maya.api.OpenMaya as OpenMaya2
        import pymel.core as pmc
        from common.mayautils import tuple_to_mpoint2, py_to_array

        self._prep_walk_nums()
        self._make_triangles()

        mesh = OpenMaya2.MFnMesh()
        try:
            points_array = OpenMaya2.MPointArray(self._verts)
        except TypeError:
            # I dont know why it doesnot work with Maya2016... maybe it is fixed in 2016ext2. duno - patching.
            points_array = py_to_array(self._verts, OpenMaya2.MPointArray, tuple_to_mpoint2)

        mesh.create(points_array,
                    [3]*(len(self._inds)/3),
                    self._inds)

        mesh.updateSurface()
        pmc.sets('initialShadingGroup', edit=True, forceElement=mesh.name())
        return pmc.PyNode(mesh.name())

    def _prep_walk_nums(self):
        self._walk_nums = []
        inverted = self.inverted == False
        for x in range(0, self._w):
            row = []
            for y in range(0, self._h):
                num = self._map[x][y] == inverted
                num <<= 1
                num += self._map[x+1][y] == inverted
                num <<= 1
                num += self._map[x + 1][y+1] == inverted
                num <<= 1
                num += self._map[x ][y + 1] == inverted
                row.append(num)
            self._walk_nums.append(row)

    def _make_triangles(self):
        self._verts = []
        self._inds = []

        def add_triangle( *points):
            for p in points:
                try:
                    ind = self._verts.index(p)
                except ValueError:
                    ind = len(self._verts)
                    self._verts.append( p )
                self._inds.append(ind)

        def mesh_from_points( *points):
            cnt = len(points)
            if cnt >= 3:
                add_triangle(points[0], points[1], points[2])
            if cnt >= 4:
                add_triangle(points[0], points[2], points[3])
            if cnt >= 5:
                add_triangle(points[0], points[3], points[4])
            if cnt >= 6:
                add_triangle(points[0], points[4], points[5])

        def get_outline_edges():
            edge_occurences = {}
            for i in range(0, len(self._inds) / 3):
                a = self._inds[3 * i]
                b = self._inds[3 * i + 1]
                c = self._inds[3 * i + 2]

                pairs = []
                # fixit: we are douing some edges ind different order
                for a,b in [(a,b), (b, c), (a, c)]:
                    if a>b:
                        pairs.append((a, b))
                    else:
                        pairs.append((b, a))

                edges = ['%d_%d' % (beg, end) for beg,end in pairs]
                for e in edges:
                    try:
                        edge_occurences[e] += 1
                    except KeyError:
                        edge_occurences[e] = 1

            for key, edge_ocurance in edge_occurences.items():
                if edge_ocurance > 1:
                    del edge_occurences[key]

            logger.debug(edge_occurences)
            return edge_occurences

        def extrude(_a, _b, bump):
            a = self._verts[_a]
            b = self._verts[_b]

            a2 = (a[0], a[1], a[2] + bump)
            b2 = (b[0], b[1], b[2] + bump)

            mesh_from_points(a, b, b2, a2)

        z = 0.
        d = self.size / 2.
        for i in xrange( self._w  ):
            for j in xrange(self._h):

                opp = self._walk_nums[i][j]
                x = d*(2*i - self._w)
                y = d*(2*j - self._h+1)

                if opp == 8:
                    mesh_from_points((x-d, y, z), (x, y-d, z), (x-d, y-d, z))
                elif opp == 4:
                    # mesh_from_points(bottomRight, centreBottom, centreRight);
                    mesh_from_points((x+d, y-d, z), (x, y-d, z), (x+d, y, z))
                elif opp == 2:
                    # mesh_from_points(topRight, centreRight, centreTop);
                    mesh_from_points((x+d, y+d, z), (x+d, y, z), (x, y+d, z))
                elif opp == 1:
                    # mesh_from_points(topLeft, centreTop, centreLeft);
                    mesh_from_points((x-d, y+d, z), (x, y+d, z), (x-d, y, z))

                elif opp == 12:
                    # mesh_from_points(centreRight, bottomRight, bottomLeft, centreLeft);
                    mesh_from_points((x+d, y, z), (x+d, y-d, z), (x-d, y-d, z), (x-d, y, z))
                elif opp == 6:
                    # mesh_from_points(centreTop, topRight, bottomRight, centreBottom);
                    mesh_from_points((x, y+d, z), (x+d, y+d, z), (x+d, y-d, z), (x, y-d, z))
                elif opp == 9:
                    # mesh_from_points(topLeft, centreTop, centreBottom, bottomLeft);
                    mesh_from_points((x-d, y+d, z), (x, y+d, z), (x, y-d, z), (x-d, y-d, z))
                elif opp == 3:
                    # mesh_from_points(topLeft, topRight, centreRight, centreLeft);
                    mesh_from_points((x-d, y+d, z), (x+d, y+d, z), (x+d, y, z), (x-d, y, z))

                elif opp == 10:
                    # mesh_from_points(centreTop, topRight, centreRight, centreBottom, bottomLeft, centreLeft);
                    mesh_from_points((x, y+d, z), (x+d, y+d, z), (x+d, y, z), (x, y-d, z), (x-d, y-d, z), (x-d, y, z))
                elif opp == 5:
                    # mesh_from_points(topLeft, centreTop, centreRight, bottomRight, centreBottom, centreLeft);
                    mesh_from_points((x-d, y+d, z), (x, y+d, z), (x+d, y, z), (x+d, y-d, z), (x, y-d, z), (x-d, y, z))

                elif opp == 14:
                    # mesh_from_points(centreTop, topRight, bottomRight, bottomLeft, centreLeft);
                    mesh_from_points((x, y+d, z), (x+d, y+d, z), (x+d, y-d, z), (x-d, y-d, z), (x-d, y, z))
                elif opp == 13:
                    # mesh_from_points(topLeft, centreTop, centreRight, bottomRight, bottomLeft);
                    mesh_from_points((x-d, y+d, z), (x, y+d, z), (x+d, y, z), (x+d, y-d, z), (x-d, y-d, z))
                elif opp == 11:
                    # mesh_from_points(topLeft, topRight, centreRight, centreBottom, bottomLeft);
                    mesh_from_points((x-d, y+d, z), (x+d, y+d, z), (x+d, y, z), (x, y-d, z), (x-d, y-d, z))
                elif opp == 7:
                    # mesh_from_points(topLeft, topRight, bottomRight, centreBottom, centreLeft);
                    mesh_from_points((x-d, y+d, z), (x+d, y+d, z), (x+d, y-d, z), (x, y-d, z), (x-d, y, z))

                elif opp == 15:
                    # mesh_from_points(topLeft, topRight, bottomRight, bottomLeft);
                    mesh_from_points((x-d, y+d, z), (x+d, y+d, z), (x+d, y-d, z), (x-d, y-d, z))

        if not self.walls:
            return

        edges = get_outline_edges()
        bump = d/2.
        if self.inverted:
            bump = -bump

        for edge in edges:
            a, b = edge.split('_')
            a = int(a)
            b = int(b)
            self._verts[a] = (self._verts[a][0], self._verts[a][1], bump)
            self._verts[b] = (self._verts[b][0], self._verts[b][1], bump)
            extrude(a,b,8*bump)


class CaveMaker(object):
    width = 30
    height = 20
    size = 1.
    random_fill_percent = 42
    smooth_iterations = 2
    use_random_seed = True
    seed = None
    inverted = False
    walls = True
    _map = [[]]

    def generate_map(self):
        self._map = [x[:] for x in [[0] * self.height] * self.width]
        self.random_fill_map()
        for i in range(self.smooth_iterations):
            self.smooth_map()

        # logger.debug(self)

    def gnerate_mesh(self):
        self.generate_map()
        g = GeoMaker(self._map)
        g.size = self.size
        g.inverted = self.inverted
        g.walls = self.walls
        return g.generate_mesh()

    def random_fill_map(self):
        if self.use_random_seed:
            self.seed = str(int(time.time()*1000))

        random.seed(self.seed)
        logger.debug(self.seed)

        for x in range(0,self.width):
            for y in range(0, self.height):
                if x == 0 or y == 0 or x == (self.width - 1) or y == (self.height - 1):
                    self._map[x][y] = 1
                else:
                    self._map[x][y] = int ( random.uniform(0,100) < self.random_fill_percent)

    def smooth_map(self):
        for x in range(0, self.width):
            for y in range(0, self.height):
                wall_count = self.surrounding_wall_count(x,y)
                if wall_count > 4:
                    self._map[x][y] = 1
                elif wall_count < 4:
                    self._map[x][y] = 0

    def surrounding_wall_count(self, grid_x, grid_y):
        wall_count = 0
        neighbour_range = 1
        for nx in range(grid_x-neighbour_range, grid_x+neighbour_range+1):
            for ny in range(grid_y - neighbour_range, grid_y + neighbour_range + 1):
                if nx == grid_x and ny == grid_y:
                    continue
                if nx <= 0 or ny <= 0 or nx >= self.width -1 or ny >= self.height -1:
                    wall_count += 1
                else:
                    wall_count += self._map[nx][ny]
        return wall_count

    def __str__(self):
        out = ""
        for x in range(0, self.width):
            for y in range(0, self.height):
                out += str(self._map[x][y])
            out += "\n"
        return out

    def get_helpers(self):
        import pymel.core as pmc
        generated = []
        d = pmc.polyCube(h=self.size, w=self.size, d=self.size)
        dx = -self.size*self.width/2.
        dy = -self.size*self.height/2.
        for x in range(0, self.width):
            for y in range(0, self.height):
                if self._map[x][y] == 1:
                    d3 = pmc.duplicate(d[0])
                    pmc.move(d3[0], dx+x*self.size , dy+y*self.size)
                    generated.append(d3)

        pmc.delete(d)
        return generated

