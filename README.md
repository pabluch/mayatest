# README #

### What is this repository for? ###
* inker_maya generates geometry in engraving style from grayscale images, for now just along one default spiral curve.
[http://pawellukaszewicz.com/pl/2016/11/14/maya-slady-tuszu/] (this is maya equivalent to [http://pawellukaszewicz.com/pl/2016/10/04/przetwarzanie-obrazu-processing/] filter)
* cavegen_maya will generate cave shapes in maya, [http://pawellukaszewicz.com/maya-cavegenerator/]

* Why: Trying to learn pymel, maya python api and pyqt. Trying to keep it tidy and ensure it works in maya 2016, 2017 and with both old and new maya pytnon apis
* Version 0.02

### Thanks to: ###
* pyqt4 <--> pyqt5 made using Fredri Aaverpil's Qt.py [https://fredrikaverpil.github.io/2016/07/25/developing-with-qt-py/]
* Lovely book by Robert Galanakis [https://www.packtpub.com/hardware-and-creative/practical-maya-programming-python]
* and Unity Procedural Cave Generation tutorial by Sebastian Lague


### How do I get set up? ###

* After downloading you can add this to your shelf to make cave generator work:

```
#!python


import sys 
sys.path.append( 'path to the repository eg: d:/programming/maya/toolpack' )
import cavegen_maya
cavegen_maya.show()

import sys
sys.path.append( 'path to the repository eg: d:/programming/maya/toolpack' )
import inker_maya
inker_maya.show()
```


* Configuration

* Dependencies
pyqt4 <--> pyqt5 made using Fredri Aaverpil's Qt.py
[https://fredrikaverpil.github.io/2016/07/25/developing-with-qt-py/]


### TO DO CAVES ###

* Geometrty: uv's, intermediate points with some noise, make geometry nicer, opimize a bit: probably edge detection

* Map add all the stuff from unity procedural cave tutorial (room conectivity, removal of small rooms )

* Fix: vetex order in edges when crating geometry. Now correcting normals on walls to fix direction.

### TO DO INKER ###

* Sample the immage at sampling points to determine the width of the triangle strip - needs aliasing.
* nicer flow of the engraving now those triangles are like "play" buttons
* more spirals
* fold and unfold, but that would have to be a shape node with... 


### Who do I talk to? ###

pablo.lukaszewicz@gmail.com