import maya.OpenMaya as OpenMaya
import pymel.core as pmc
import logging

import eshergen_gui
from common.mayautils import get_maya_window
from patterns import generate_esher_1

logger = logging.getLogger()

generated = []


def tidy_up():
    # import maya.cmds as cmds
    # cmds.select(cmds.ls(dag=1, ap=1,  type='mesh'))
    # cmds.delete()
    for item in generated:
        try:
            pmc.delete(item)
        except pmc.general.MayaNodeError:
            logger.warning("allready deleted: {}".format(item))
    del generated[:]


def generate(args):
    generated.extend(generate_esher_1(args))


_window = None


def show():
    global _window
    if _window is None:
        cont = eshergen_gui.EsherController()
        _window = eshergen_gui.create_window(cont, get_maya_window())

        def emit_selchanged(_):
            cont.selectionChanged.emit(pmc.selected(type='transform'))

        OpenMaya.MEventMessage.addEventCallback('SelectionChanged', emit_selchanged)
        _window.generateClicked.connect(generate)
        _window.delClicked.connect(tidy_up)

    _window.show()
