import logging
import math
from ctypes import *

import maya.api.OpenMaya as OpenMaya2
import pymel.core as pmc
from common.mayautils import tuple_to_mpoint2, py_to_array

logger = logging.getLogger()


class GeoMaker(object):

    _verts = []
    _inds = []
    _pattern = []
    width = 1.
    height = 1.
    path = ""
    step = 0.001
    _image = None
    _pw = 0
    _ph = 0

    def load_image(self):
        image = OpenMaya2.MImage()
        self._image = image.readFromFile( self.path)
        self._pw, self._ph = image.getSize()

        logger.debug(self._image)

    def __init__(self, _pat):
        self._pattern = _pat

    def generate_mesh(self):

        self._verts = []
        self._inds = []
        # for p in :
        #     self._verts.append((p[0]*self.width, p[1]*self.height, p[2]))

        self.load_image()
        px = cast(self._image.pixels(), POINTER(c_ubyte))
        p = self._pattern
        last_ind = -1
        dd = self.step

        logger.debug( self._pw)
        coll_sample = 255
        coll_sample_last = 255
        trash = 120
        count_array = []

        for a in range(1,len( self._pattern)):
            x = min( int((0.5 + p[a][0]) * self._pw), self._pw-1)
            y = min( int((0.5 + p[a][1]) * self._ph), self._ph-1)
            xy = math.sqrt( p[a][0]**2+p[a][1]**2)
            coll_sample = px[y * 4 * self._pw + x * 4]
            dd2 = dd * (0.1 + coll_sample/255.)

            dy = p[a][1]/xy*dd2
            dx = p[a][0]/xy*dd2

            if coll_sample < trash and coll_sample_last >= trash:
                #   *
                # *
                #   *
                self._verts.append((p[a-1][0] * self.width, p[a-1][1] * self.height, p[a-1][2]))
                self._verts.append( ((p[a][0] -dx)* self.width, (p[a][1]-dy) * self.height, p[a][2]))
                self._verts.append( ((p[a][0] +dx)* self.width, (p[a][1]+dy) * self.height, p[a][2]))

                self._inds.append(last_ind + 3)
                self._inds.append(last_ind + 2)
                self._inds.append(last_ind + 1)
                count_array.append(3)
                last_ind += 3

            elif coll_sample >= trash and coll_sample_last < trash:
                #   *
                #     *
                #   *
                self._verts.append((p[a][0] * self.width, p[a][1] * self.height, p[a][2]))
                self._inds.append(last_ind + 1)
                self._inds.append(last_ind - 1)
                self._inds.append(last_ind )
                count_array.append(3)
                last_ind += 1

            elif coll_sample < trash and coll_sample_last < trash:
                #   *   *
                #
                #   *   *
                self._verts.append(((p[a][0] - dx) * self.width, (p[a][1] - dy) * self.height, p[a][2]))
                self._verts.append(((p[a][0] + dx) * self.width, (p[a][1] + dy) * self.height, p[a][2]))

                self._inds.append(last_ind - 1)
                self._inds.append(last_ind )
                self._inds.append(last_ind + 2)
                self._inds.append(last_ind + 1)
                count_array.append(4)
                last_ind += 2

            coll_sample_last = coll_sample
            #logger.debug(coll_sample)

            # if a == 0:
            #     self._verts.append( (p[a][0] * self.width, p[a][1] * self.height, p[a][2]) )
            # elif a == 1:
            #     self._verts.append( ((p[a][0] -dx)* self.width, (p[a][1]-dy) * self.height, p[a][2]))
            #     self._verts.append( ((p[a][0] +dx)* self.width, (p[a][1]+dy) * self.height, p[a][2]))
            #     self._verts.append( (p[a][0] * self.width, p[a][1] * self.height, p[a][2]))
            #     self._inds.append(0)
            #     self._inds.append(1)
            #     self._inds.append(3)
            #     self._inds.append(2)
            #     count_array.append(4)
            #     last_ind = 3
            # else:
            #     self._verts.append( ((p[a][0] -dx)* self.width, (p[a][1]-dy) * self.height, p[a][2]))
            #     self._verts.append( ((p[a][0] +dx)* self.width, (p[a][1]+dy) * self.height, p[a][2]))
            #     self._verts.append( (p[a][0] * self.width, p[a][1] * self.height, p[a][2]))
            #     self._inds.append(last_ind)
            #     self._inds.append(last_ind+ 1)
            #     self._inds.append(last_ind+ 3)
            #     self._inds.append(last_ind+ 2)
            #     count_array.append(4)
            #     last_ind += 3

        mesh = OpenMaya2.MFnMesh()
        try:
            points_array = OpenMaya2.MPointArray(self._verts)
        except TypeError:
            points_array = py_to_array(self._verts, OpenMaya2.MPointArray, tuple_to_mpoint2)

        logger.debug( len(self._verts))
        logger.debug(len(self._inds))
        logger.debug(coll_sample)

        # mesh.create(points_array, [3]*(len(self._inds)/3), self._inds)
        #mesh.create(points_array, [4] * (len(self._inds) / 4), self._inds)
        mesh.create(points_array, count_array, self._inds)

        mesh.updateSurface()
        pmc.sets('initialShadingGroup', edit=True, forceElement=mesh.name())
        return pmc.PyNode(mesh.name())

    def _make_triangles(self):
        self._verts = []
        self._inds = []

        def add_triangle( *points):
            for p in points:
                try:
                    ind = self._verts.index(p)
                except ValueError:
                    ind = len(self._verts)
                    self._verts.append( p )
                self._inds.append(ind)

        def mesh_from_points( *points):
            cnt = len(points)
            if cnt >= 3:
                add_triangle(points[0], points[1], points[2])
            if cnt >= 4:
                add_triangle(points[0], points[2], points[3])
            if cnt >= 5:
                add_triangle(points[0], points[3], points[4])
            if cnt >= 6:
                add_triangle(points[0], points[4], points[5])

        def get_outline_edges():
            edge_occurences = {}
            for i in range(0, len(self._inds) / 3):
                a = self._inds[3 * i]
                b = self._inds[3 * i + 1]
                c = self._inds[3 * i + 2]

                pairs = []
                # fixit: we are douing some edges ind different order
                for a,b in [(a,b), (b, c), (a, c)]:
                    if a>b:
                        pairs.append((a, b))
                    else:
                        pairs.append((b, a))

                edges = ['%d_%d' % (beg, end) for beg,end in pairs]
                for e in edges:
                    try:
                        edge_occurences[e] += 1
                    except KeyError:
                        edge_occurences[e] = 1

            for key, edge_ocurance in edge_occurences.items():
                if edge_ocurance > 1:
                    del edge_occurences[key]

            logger.debug(edge_occurences)
            return edge_occurences

        def extrude(_a, _b, bump):
            a = self._verts[_a]
            b = self._verts[_b]

            a2 = (a[0], a[1], a[2] + bump)
            b2 = (b[0], b[1], b[2] + bump)

            mesh_from_points(a, b, b2, a2)

        z = 0.
        d = self.size / 2.
        for i in xrange( self._w  ):
            for j in xrange(self._h):

                opp = self._walk_nums[i][j]
                x = d*(2*i - self._w)
                y = d*(2*j - self._h+1)


class PatternSpiral1(object):
    step = 0.01
    spins = 10
    _points = []
    K = -2.15778299

    def make(self):
        self._points = []
        # spans = 10
        #
        # cnt = self.spins * spans
        # for i in xrange( cnt ):
        #     l = (0.5 * i)/cnt
        #     a = (i%spans*math.pi*2)/spans
        #     self._points.append( ( math.sin(a)*l, math.cos(a)*l,0))

        steps = int( 0.25*(2*math.pi*self.spins - self.K)**2)
        a = l = 0
        l_scale = .5/math.sqrt( steps )
        for i in xrange(steps):
            if i ==0:
                a = 0
                l = 1
            else:
                l = math.sqrt(i)
                a += math.asin(1./l)

            l *= l_scale
            self._points.append((math.sin(a) * l, math.cos(a) * l, 0))

        logger.debug( self._points )
        return self._points


class Inktober(object):
    path = "(empty)"
    width = 1.
    height = 1.
    step = 0.001

    pattern_maker = None
    _pattern = None

    def make_geo(self):
        self._pattern = []
        self._pattern = self.pattern_maker.make()

        g = GeoMaker( self._pattern )

        g.width = self.width
        g.height = self.height
        g.path = self.path
        g.step = self.step

        return g.generate_mesh()




