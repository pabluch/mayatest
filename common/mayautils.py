import maya.OpenMayaUI as OpenMayaUI
import pymel.core as pmc
import maya.cmds as cmds


def get_maya_window():

    """Return the QMainWindow for the main Maya window."""
    try:
        from PySide2 import QtWidgets, QtCore

        for obj in QtWidgets.qApp.topLevelWidgets():
            if obj.objectName() == 'MayaWindow':
                return obj
        raise RuntimeError('No Maya window found.')

    except ImportError:
        from qtshim import wrapinstance, QtGui
        winptr = OpenMayaUI.MQtUtil.mainWindow()
        if winptr is None:
            raise RuntimeError('No Maya window found.')
        window = wrapinstance(winptr)
        assert isinstance(window, QtGui.QMainWindow)
        return window


def get_main_window_name():
    return pmc.MelGlobals()['gMainWindow']


def uipath_to_qtobject(pathstr):
    """Return the QtObject for a Maya UI path to a control,
    layout, or menu item.
    Return None if no item is found.
    """
    ptr = OpenMayaUI.MQtUtil.findControl(pathstr)
    if ptr is None:
        ptr = OpenMayaUI.MQtUtil.findLayout(pathstr)
    if ptr is None:
        ptr = OpenMayaUI.MQtUtil.findMenuItem(pathstr)
    if ptr is not None:
        return wrapinstance(ptr)
    return None


def py_to_array(values, marray_type, projection=None):
    """Convery python arays to OpenMaya types eg:
    vert_pos_array = py_to_array(vert_positions, OpenMaya.MPointArray,tuple_to_mpoint)
    poly_counts_array = py_to_array(poly_counts, OpenMaya.MIntArray)
    poly_conns_array = py_to_array(poly_connections, OpenMaya.MIntArray)
    """
    result = marray_type()
    for v in values:
        newv = v
        if projection is not None:
            newv = projection(v)
        result.append(newv)
    return result


def tuple_to_mpoint(p):
    from maya import OpenMaya
    return OpenMaya.MPoint(p[0], p[1], p[2])


def tuple_to_mpoint2(p):
    from maya.api import OpenMaya
    return OpenMaya.MPoint(p[0], p[1], p[2])


def name_to_mobject(name):
    from maya import OpenMaya
    sellist = OpenMaya.MSelectionList()
    sellist.add(name)
    node = OpenMaya.MObject()
    sellist.getDependNode(0, node)
    return node


class UndoContext(object):

    def __enter__(self):
        cmds.undoInfo(openChunk=True)

    def __exit__(self, *exc_info):
        cmds.undoInfo(closeChunk=True)

