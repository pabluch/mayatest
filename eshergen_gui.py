from common.Qt import QtGui, QtCore, QtWidgets


class EsherWindow(QtWidgets.QMainWindow):
    generateClicked = QtCore.Signal(tuple)
    delClicked = QtCore.Signal()


class EsherController(QtCore.QObject):
    selectionChanged = QtCore.Signal(list)


def create_window(controller, parent=None):
    window = EsherWindow(parent)
    window.setWindowTitle('Esher generator')
    statusbar = QtWidgets.QStatusBar()
    window.setStatusBar(statusbar)

    container = QtWidgets.QWidget(window)
    layout = QtWidgets.QGridLayout(container)
    container.setLayout(layout)
    window.setCentralWidget(container)

    label = QtWidgets.QLabel('Width:', container)
    textbox = QtWidgets.QLineEdit(container)
    textbox.setText('8')
    layout.addWidget(label, 0, 0)
    layout.addWidget(textbox, 0, 1)
    tW = textbox

    label = QtWidgets.QLabel('Height:', container)
    textbox = QtWidgets.QLineEdit(container)
    textbox.setText('6')
    layout.addWidget(label, 1, 0)
    layout.addWidget(textbox, 1, 1)
    tH = textbox

    label = QtWidgets.QLabel('Size:', container)
    textbox = QtWidgets.QLineEdit(container)
    textbox.setText('1.0')
    layout.addWidget(label, 2, 0)
    layout.addWidget(textbox, 2, 1)
    tX = textbox

    button = QtWidgets.QPushButton('Del', container)
    layout.addWidget(button, 3, 0)
    buttonD = button

    button = QtWidgets.QPushButton('Generate', container)
    layout.addWidget(button, 3, 1)
    buttonG = button

    def onclick():
        tup = [tb.text() for tb in [tW, tH, tX]]
        window.generateClicked.emit(tup)

    def tidy_up():
        window.delClicked.emit()

    def update_statusbar(newsel):  # (4)
        if not newsel:
            txt = 'Nothing selected.'
        elif len(newsel) == 1:
            txt = '"%s" selected.' % newsel[0]
        else:
            txt = '%s objects selected.' % len(newsel)
        window.statusBar().showMessage(txt)

    controller.selectionChanged.connect(update_statusbar)

    buttonG.clicked.connect(onclick)
    buttonD.clicked.connect(tidy_up)

    return window


def _pytest():
    import random

    def ongenerate(args):
        def test_sel():
            sels = [
                [],
                ['single'],
                ['single', 'double'],
                ['single', 'double', 'triple'],
            ]
            return random.choice(sels)

        cont.selectionChanged.emit(test_sel())
        # generateEsher( *args)

    try:
        app = QtGui.QApplication([])
    except:
        app = QtGui.QApplication.instance()

    cont = EsherController()
    win = create_window(cont)
    win.generateClicked.connect(ongenerate)
    win.show()
    app.exec_()


if __name__ == '__main__':
    _pytest()
