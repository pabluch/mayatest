from common.mayautils import *
from maya import OpenMaya
import maya.api.OpenMaya as OpenMaya2
import pymel.core as pmc

import logging
logger = logging.getLogger()

# Book code:
vert_positions = [
    (1, 1, 1), (1, -1, 1), (-1, -1, 1), (-1, 1, 1),
    (1, 1, 1), (1, -1, 1), (1, -1, -1), (1, 1, -1),
    (1, 1, -1), (1, -1, -1), (-1, -1, -1), (-1, 1, -1),
    (-1, 1, 1), (-1, -1, 1), (-1, -1, -1), (-1, 1, -1),
    (1, 1, 1), (1, 1, -1), (-1, 1, -1), (-1, 1, 1),
    (1, -1, 1), (1, -1, -1), (-1, -1, -1), (-1, -1, 1),
]
# all faces will have same UVs
vert_uvs = [(0, 0), (0, 1), (1, 1), (1, 0)] * 6
ulist, vlist = zip(*vert_uvs)
poly_counts = [4] * 6
poly_connections = [
    3, 2, 1, 0,
    4, 5, 6, 7,
    8, 9, 10, 11,
    15, 14, 13, 12,
    16, 17, 18, 19,
    23, 22, 21, 20
]
# Book code ends

# New easy API:


def create_cube_new_api_1():
    mesh = OpenMaya2.MFnMesh()
    try:
        points_array = OpenMaya2.MPointArray(vert_positions)
    except TypeError:
        # I dont know why it doesnot work with Maya2016... maybe it is fixed in 2016ext2. duno - patching.
        points_array = py_to_array(vert_positions, OpenMaya2.MPointArray, tuple_to_mpoint2)

    mesh.create(
        points_array,
        poly_counts,
        poly_connections
    )
    mesh.setUVs(ulist, vlist)
    mesh.assignUVs(poly_counts, poly_connections)
    mesh.updateSurface()
    pmc.sets(
        'initialShadingGroup',
        edit=True, forceElement=mesh.name()) #(6)

    return pmc.PyNode(mesh.name())


# For old API conversions....
# Book code:
vert_pos_array = py_to_array(vert_positions, OpenMaya.MPointArray, tuple_to_mpoint)
poly_counts_array = py_to_array(poly_counts, OpenMaya.MIntArray)
poly_conns_array = py_to_array(poly_connections, OpenMaya.MIntArray)
uarray = py_to_array(ulist, OpenMaya.MFloatArray)
varray = py_to_array(vlist, OpenMaya.MFloatArray)


def create_cube_1():
    mesh = OpenMaya.MFnMesh()
    mesh.create(
        len(vert_positions),
        len(poly_counts ),
        vert_pos_array,
        poly_counts_array,
        poly_conns_array
    )
    mesh.setUVs(uarray, varray)
    mesh.assignUVs(poly_counts_array, poly_conns_array)
    mesh.updateSurface()
    pmc.sets(
        'initialShadingGroup',
        edit=True, forceElement=mesh.name())

    return pmc.PyNode(mesh.name())


def get_normals_data():
    result = {}
    offset = 0
    for i, pcnt in enumerate(poly_counts):
        vertInds = poly_connections[offset:offset + pcnt]
        positions = [vert_positions[vind] for vind in vertInds]
        normals = [OpenMaya.MVector(p[0], p[1], p[2]).normal()
                   for p in positions]
        result[i] = (vertInds, normals)
        offset += pcnt
    return result

face_to_vert_inds_and_normals = get_normals_data()


def create_cube_2():
    """ With normalas, smothed as if it was a sphere """
    mesh = OpenMaya.MFnMesh()
    mesh.create(
        len(vert_positions),
        len(poly_counts),
        vert_pos_array,
        poly_counts_array,
        poly_conns_array
    )
    mesh.setUVs(uarray, varray)
    mesh.assignUVs(poly_counts_array, poly_conns_array)

    items = face_to_vert_inds_and_normals.items()
    for faceInd, (vertInds, norms) in items:
        for vind, normal in zip(vertInds, norms):
            mesh.setFaceVertexNormal(normal, faceInd, vind)

    mesh.updateSurface()
    pmc.sets(
        'initialShadingGroup',
        edit=True, forceElement=mesh.name())

    return pmc.PyNode(mesh.name())


