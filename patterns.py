import math
import pymel.core as pmc


def generate_esher_1(args):
    generated =[]
    w, h, x = args
    w = int(w)
    h = int(h)
    x = float(x)

    dx = 2*x/math.sqrt(2)
    dy = x
    dz = x/math.sqrt(2)

    d = pmc.polyCube(h=x,w=x,d=x)

    generated.append(d)
    pmc.rotate(d,[0,45,0])
    for i in range(0,h):
        for j in range(0,w):
            d3 = pmc.duplicate(d[0])
            pmc.move(d3[0],[dx*(j+(i%2)/2.),-dy*i,dz*i])
            generated.append(d3)

    return generated