import maya.OpenMaya as OpenMaya
import pymel.core as pmc
import maya.cmds as cmds
import logging

import cavegen_gui
from common.mayautils import get_maya_window, UndoContext
from boxing_demo import create_cube_2, create_cube_new_api_1
from patterns import generate_esher_1


logger = logging.getLogger()
generated = []


def handle_generate(args):
    global _window
    with UndoContext():
        if args.opp == cavegen_gui.CaveWindow.GEN_CAVE0 or args.opp == cavegen_gui.CaveWindow.GEN_CAVE1:
            tidy_up()
            from cavemaker import CaveMaker
            p = CaveMaker()
            p.size = args.size
            p.width = args.width
            p.height = args.height
            p.random_fill_percent = args.fill_p
            p.smooth_iterations = args.s_iter
            p.inverted = args.inv
            p.use_random_seed = args.random
            p.seed = args.seed
            p.walls = args.walls

            if args.opp == cavegen_gui.CaveWindow.GEN_CAVE1:
                geo = p.gnerate_mesh()
                generated.append(geo )
                pmc.polyNormal(geo, nm=2, unm=0,ch=0)
                pmc.polySoftEdge(geo, a=0, ch=0)
                cmds.select(clear=True)
            else:
                p.generate_map()
                geo = p.get_helpers()
                generated.extend(geo)

            _window.seed.setText(str(p.seed))

        elif args.opp == cavegen_gui.CaveWindow.GEN_CUBE_OLD:
            geo = create_cube_2();
            logger.debug('BOX {}'.format(geo))
            generated.append(geo)

        elif args.opp == cavegen_gui.CaveWindow.GEN_CUBE_20:
            geo = create_cube_new_api_1()
            logger.debug('BOX 2.0 {}'.format(geo))
            generated.append(geo)

        elif args.opp == cavegen_gui.CaveWindow.GEN_PAT_1:
            geo = generate_esher_1( (args.width, args.height, args.size))
            logger.debug('PAT 1')
            generated.append(geo)


def tidy_up():
    with UndoContext():
        for item in generated:
            try:
                pmc.delete(item)
            except pmc.general.MayaNodeError:
                logger.warning("allready deleted: {}".format(item))
        del generated[:]


_window = None


def show():
    global _window
    if _window is None:
        cont = cavegen_gui.CaveController()
        _window = cavegen_gui.create_window(cont, get_maya_window())

        def emit_selchanged(_):
            cont.selectionChanged.emit(pmc.selected(type='transform'))

        OpenMaya.MEventMessage.addEventCallback('SelectionChanged', emit_selchanged)
        _window.generateClicked.connect( handle_generate )
        _window.delClicked.connect(tidy_up)

    _window.show()
