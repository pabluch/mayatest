import maya.OpenMaya as OpenMaya
import pymel.core as pmc
import maya.cmds as cmds
import logging
import inker_gui, inker

from common.mayautils import get_maya_window, UndoContext

logger = logging.getLogger()
generated = []


def handle_generate(args):
    global _window
    with UndoContext():
        inktober = inker.Inktober()
        inktober.width = args.width
        inktober.height = args.height
        inktober.path = args.filepath
        inktober.step = args.step

        if args.opp == inker_gui.UiWindow.GEN_SPIRAL1:
            tidy_up()
            logger.debug(args)

            pattern = inker.PatternSpiral1()
            pattern.spins = args.spins
            pattern.step = args.step

            inktober.pattern_maker = pattern

        generated.append(inktober.make_geo())

def tidy_up():
    with UndoContext():
        for item in generated:
            try:
                pmc.delete(item)
            except pmc.general.MayaNodeError:
                logger.warning("allready deleted: {}".format(item))
        del generated[:]


_window = None


def show():
    global _window
    if _window is None:
        cont = inker_gui.UiController()
        _window = inker_gui.create_window(cont, get_maya_window())

        def emit_selchanged(_):
            cont.selectionChanged.emit(pmc.selected(type='transform'))

        OpenMaya.MEventMessage.addEventCallback('SelectionChanged', emit_selchanged)
        _window.generateClicked.connect( handle_generate )
        _window.delClicked.connect(tidy_up)

    _window.show()
