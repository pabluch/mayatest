#from PySide2 import QtGui, QtCore, QtWidgets
from common.Qt import QtGui, QtCore, QtWidgets
from functools import partial
import sys, os, fnmatch
import threading
from Queue import Queue

class CdpListItem(QtGui.QStandardItem):
   def __init__(self, *args, **kwargs):
      QtGui.QStandardItem.__init__(self, *args, **kwargs)
      self.tags = set(os.path.dirname(args[0]).split('/'))

class MultiwordCompleter(QtWidgets.QCompleter):
   # from: https://stackoverflow.com/questions/35628257/pyqt-auto-completer-with-qlineedit-multiple-times
   def __init__(self, *args, **kwargs):
      QtWidgets.QCompleter.__init__(self, *args, **kwargs)
      self.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
      self.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
      self.setWrapAround(False)

   def pathFromIndex(self, index):
      path = QtWidgets.QCompleter.pathFromIndex(self, index)
      lst = str(self.widget().text()).split(' ')
      if len(lst) > 1:
         path = '%s %s' % (' '.join(lst[:-1]), path)
      return path

   def splitPath(self, path):
      path = str(path.split(' ')[-1]).lstrip(' ')
      return [path]

class CdpWidget(QtWidgets.QWidget):
   def __init__(self, path=None, parent=None):
      QtWidgets.QWidget.__init__(self, parent=parent)
      self.path = path if path else os.getcwd()
      self.path.rstrip('/\\')

      lay = QtWidgets.QVBoxLayout(self)
      lay.setSpacing(2)
      lay.setContentsMargins(5,5,5,5)
      lay.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
      self.wFilter = QtWidgets.QLineEdit()
      self.wFilter.setPlaceholderText('Filter tags (space separated). Pres ENTER to filter.')
      self.wFilter.returnPressed.connect(self.filterChanged)

      self.wStatus = QtWidgets.QStatusBar()

      self.filterSet = set()
      self.filterPattern = "*"

      self.filterCompleter = MultiwordCompleter()
      self.wFilter.setCompleter(self.filterCompleter)
      self.filterCompleterModel = QtCore.QStringListModel()
      # self.filterCompleterModel = QtGui.QStringListModel()
      self.filterCompleter.setModel(self.filterCompleterModel)

      self.wPath = QtWidgets.QLineEdit(self.path)
      self.wPath.textChanged.connect(self.pathChanged)

      self.wList = QtWidgets.QListView()
      self.wList.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
      self.wList.customContextMenuRequested.connect(self.openMenu)
      self.wList.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

      self.model = QtGui.QStandardItemModel(self.wList)
      self.wList.setModel(self.model)

      self.populateList()
      self.setMinimumWidth(400)

      lay.addWidget(self.wPath)
      lay.addWidget(self.wFilter)
      lay.addWidget(self.wList)
      lay.addWidget(self.wStatus)
      self.wStatus.hide()
      QtCore.QTimer.singleShot(1,self.wFilter.setFocus)

   def filterChanged(self):
      self.filterSet.clear()
      self.filterSet.update(self.wFilter.text().split(' '))
      try:
         self.filterSet.remove(u'')
      except KeyError:
         pass
      for row in range(self.model.rowCount()):
         if self.filterSet.issubset(self.model.item(row).tags):
            self.wList.setRowHidden(row, False)
         else:
            self.wList.setRowHidden(row, True)

   def pathChanged(self, path):
      if os.path.isdir(path) and ( path.rstrip('/\\') !=  self.path.rstrip('/\\')):
         self.path = path.rstrip('/\\')
         self.populateList()


   def populateList(self):
      self.model.clear()
      self.hintTags = set()
      self.wStatus.show()
      self.wStatus.showMessage('Loading...')
      self.wPath.setDisabled(True)
      self.wFilter.setDisabled(True)
      self._queue = Queue()
      t = threading.Thread(target=partial(self._populateWorker))
      t.start()
      QtCore.QTimer.singleShot(10,self._populateCollector)
      
   def _populateWorker(self):
      cutoff = len(self.path)+1
      for path, dirs, files in os.walk(self.path):
         self.hintTags.update(dirs)
         for filename in fnmatch.filter(files, self.filterPattern):
            f=os.path.join(path, filename)
            self._queue.put(f[cutoff:].replace('\\','/'))
      self._queue.put(None)

   def _populateCollector(self):
      cnt = 0
      while True:
         cnt += 1
         if cnt > 100:
            break
         path = self._queue.get()
         if path is None :
            break
         item = CdpListItem(path)
         self.model.appendRow(item)

      if path is not None:
         QtCore.QTimer.singleShot(200,self._populateCollector)
      else:
         self.filterCompleterModel.setStringList(list(self.hintTags))
         self.wStatus.showMessage('Loaded.')
         QtCore.QTimer.singleShot(300,self.wStatus.hide)
         self.wPath.setDisabled(False)
         self.wFilter.setDisabled(False)
         QtCore.QTimer.singleShot(0,self.wPath.setFocus)
         self.filterChanged()


   def openMenu(self, position):
      indexes = ',\n'.join( [item.data() for item in self.wList.selectedIndexes()] )
      menu = QtWidgets.QMenu()
      for s in ["Delete","Copy path","Open"]:
         act = QtWidgets.QAction(menu)
         act.setText(s)
         act.triggered.connect( partial(self.dummyAction,indexes,s +" clicked"))
         menu.addAction(act)
      menu.exec_(self.wList.viewport().mapToGlobal(position))

   def dummyAction(self, ind, text):
      QtWidgets.QMessageBox.information(self,text,ind)

def _pytest():
   try:
      app = QtWidgets.QApplication(sys.argv)
   except:
      app = QtGui.QApplication.instance()

   win = QtWidgets.QMainWindow()
   wid = CdpWidget()
   win.setCentralWidget(wid)
   win.show()
   sys.exit(app.exec_())

if __name__ == '__main__':
   _pytest()